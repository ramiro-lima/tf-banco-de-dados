package conn;

import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.pool.OracleDataSource;

public class Conexao {

	private static String mJdbcUrl = "jdbc:oracle:thin:@camburi.pucrs.br:1521:facin11g";
	private static String mUser = "bb202278";
	private static String mPassword = "ramirou1";
	private static OracleDataSource mDataSource = null;

	public static Connection getConnection() throws SQLException {
		if (mDataSource == null) {
			initialize();
		}

		return mDataSource.getConnection(mUser, mPassword);
	}

	public static void initialize() throws SQLException {
		mDataSource = new OracleDataSource();
		mDataSource.setURL(mJdbcUrl);
	}
}
