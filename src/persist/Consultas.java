package persist;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Consultas {
	public static void select(Connection connection, int movieId) throws SQLException {
		String sql = "SELECT * " + 
					 "FROM filmes " + 
					 "WHERE id_filme = " + movieId;
		
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);

		while (rs.next()) {
			int id_filme = rs.getInt("id_filme");
			String titulo = rs.getString("titulo");
			int ano = rs.getInt("ano");
			String diretor = rs.getString("diretor");
			System.out.println(id_filme + "\t" + titulo + "\t" + ano + "\t" + diretor);
		}
		
		statement.close();
		rs.close();
	}
}


