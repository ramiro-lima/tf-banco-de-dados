package app;

import java.sql.Connection;
import java.sql.SQLException;
import conn.Conexao;
import persist.Consultas;

public class App {

	public static void main(String[] args) throws SQLException {

		Connection conn = Conexao.getConnection();	
		Consultas.select(conn, 2);
		conn.close();	
	}
}

